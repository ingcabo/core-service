const express = require('express');
const path = require('path');
const mime = require('mime');
const middlewares = require('./middlewares');
var https = require('https');
var http = require('http');
const router = require('./network/routes');
var fs = require('fs');
const HttpsPort = 443;
const {WEB_PORT_APP} = require("./config");
const HttpPort = WEB_PORT_APP;
const httpsOptions ={
    key: fs.readFileSync('./crt/serverQaService.key', 'utf8'),
    cert: fs.readFileSync('./crt/clientQaService.cert', 'utf8')
}

var app = express();
//static Server

app.use(express.static(path.join(__dirname,'/volume/public')));
middlewares(app);  
router(app);

app.options('/*',function(req,res){
    req.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    req.header("Access-Control-Allow-Methods", "*");
    req.header("Access-Control-Allow-Headers", "*");
});



/*
https.createServer(httpsOptions ,app).listen(HttpsPort, function(){
    console.log(`se esta escuchando SSL en https://localhost:${HttpsPort}`);
});
*/
// levantamos nuestro servidor en el puerto seleccionado
/*app.listen(HttpPort, () => {
    console.log(`Servidor iniciado en el puerto: ${HttpPort}`);
  });
*/
http.createServer(httpsOptions ,app).listen(HttpPort, function(){
    console.log(`ON ... http://localhost:${HttpPort}`);
});
