const { IncomingWebhook } = require('@slack/webhook');
const {ApiClient } = require('./apiClient.js')
//const WEBHOOK_URL_PATH = process.env.WEBHOOK_URL_PATH;

const {WEBHOOK_URL_PATH} = require("../config");

function slack_send_msg(message,sendTo ='URLfd0L7igtYQo2YwwjnMVhQ'){
  
    var completeUrl =`${WEBHOOK_URL_PATH}/${sendTo}`;
    console.log('URL======$$$$$$',completeUrl)
    const webhookf = new IncomingWebhook(completeUrl);
    var error='#FF0000';
    var success='#1AC107';
    var mycolor = success;
    var exist = message.indexOf('Failed Test'); 

    if (exist !== -1 ) {
        mycolor = error;
    }

    return new Promise( async (resolve,reject) =>{
       
        const result = await webhookf.send({
            type: "mrkdwn",
            text: "",
            attachments:[
                {
                text:`${message}`,
                color:mycolor,
                footer:"company",
                footer_icon:"https://images.ctfassets.net/drib7o8rcbyf/6w2Y64NVtYvHGBs8vF9a3W/64360873a9e45c623222355b69011fa0/equinox-logo.png"
            }
        ]
          });

        resolve(result);
    })
}

async function msTeams_send_msg(body){
    let BaseUrl ="https://falabella.webhook.office.com/webhookb2/66b8eb35-605c-4e89-b8fb-0bf16780d205@c4a8886b-f140-478b-ac47-249555c30afd/IncomingWebhook/"
    let sendMe="fdf0768e20dd427593c41e95f808ac9b/d135f778-4931-4339-8938-ec4eaf8f7140"
    //const MT_WEBHOOK_URL_PATH = process.env.MT_WEBHOOK_URL_PATH;
    let client = new ApiClient(BaseUrl,"","");
    var error='#FF0000';
    var success='#1AC107';
    var mycolor = success;
   // var exist = text.indexOf('Failed Test'); 

    let color=mycolor;
    let footer="Fallabela";
    let footer_icon="./logo.png";

 
    return await client.post(sendMe,body,null);
}


module.exports = {
    slack_send_msg,
    msTeams_send_msg
}


