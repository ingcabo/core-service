const fs  = require('fs');
//const cfg = require('../cfg.json');
const {dirBatchfile} = require('../config');

function delay(time) {
    return new Promise(resolve => setTimeout(resolve, time));
} 
async function generateBody(file_name){
    let packageObj = JSON.parse(this.readFile(dirBatchfile + file_name + ".json"));
    const result = packageObj.map((obj) => Object.keys(obj));
    let body = [];
    let count = result[0].length;
    result[0].forEach(element => {
        let property = [];
        if(element.search('fecha') >= 0){
            type = "date";
        }

        property = setProperty(element);

        object = {
            name: element,
            "type": property[0],
            "format": property[1],
            "max_length": property[2],
            "min_length": property[3],
        };
        body.push(object);
    });
   
    return [body, count];
}
function setProperty(element){
    let type = "";
    let format = "";
    let maxLength = "";
    let minLength = "";
    switch (true)
    {
        case ["fecha_emision", "fecha_vencimiento","fecha_cancelacion","#fec_gen_informe","fecha_vencimiento_nueva","fecha_proceso"].includes(element) :
            type = "date";
            format = "DD/MM/YYYY";
            minLength = 0;
            maxLength = 10;
        break;
        case ["FechaCorte"].includes(element) :
            type = "date";
            format = "YYYYMMDD";
            minLength = 0;
            maxLength = 8;
        break;
        case ["mes"].includes(element) :
            type = "date";
            format = "YYYYMM";
            minLength = 0;
            maxLength = 6;
        break;
        case ["nomoficina","tipoidentif","nombre_titular","estado","bloqueado","formapago","oficina","TipoPersona",
                "TipoInformacion","vendedorcdt","exprestasa","Intereses_al_vencimiento","apl1","apl2","nom1","nom2",
                "raz","dir","dpto","mun","nombre_cliente","motivobloqueo","especie","moneda"].includes(element) :
            type = "string";
            minLength = 0;
            maxLength = 255;
        break;
        case ["inversion","#inversion","nro_cdat"].includes(element) :
            type = "alphanumeric";
            format = "";
            minLength = 0;
            maxLength = 20;
        break;
        case ["numerodoc","plazo_emision","nrenovacion","SubCuenta","codoficina","tdoc","nid","dv","pais","ntit",
                "ttitu","tmov","salini","inv","intca","vintpa","retfup","salfin","tits","tipo_documento","nro_documento",
                "plazo_dias","NrodeCuenta","tipoidentifnum","numidentif","tipo_cuenta","tasa_renov"].includes(element) :
            type = "numeric";
            format = "";
            minLength = 0;
            maxLength = 20;
        break;
        case ["vrnominal","tasaefectiva","cappagado","intpagado","retefuente","Gravamen","capital","interes",
                "monto_capital","tasaEA"].includes(element) :
            type = "amount";
            format = "999999999999999,99";
            minLength = 0;
            maxLength = 18;
        break;
        case ["Valor"].includes(element) :
            type = "double";
            format = "999.999.999.999,99";
            minLength = 0;
            maxLength = 18;
        break;
        
        default:
            type = format = minLength = maxLength = "";
        break;

    }

    return [type,format,maxLength,minLength];
}

function readFilesFolder(folder=dirReport, enconding='utf-8') {
    
    return new Promise(function(resolve, reject) {
        fs.readdir(folder,enconding, function(err, filenames){
            if (err) 
                reject(err); 
            else 
                
                resolve(filenames);
        });
    });
};

//if we need to get save the data/env/collections files tests - change by aws s3 method
async function moveFile (file,patch){
    file.mv(patch + file.name);
    return patch+file.name;
}


//if we need to get read the configuration files of the tests - change by aws s3 method
function readFile(file) {
    return fs.readFileSync(file, { encoding: 'utf8', flag: 'r' });
}


//if we need to write the configuration files of the tests - change by aws s3 method
function writeFile(InputValue,file) {
    if (typeof(InputValue) === 'object'){
        InputValue = JSON.stringify(InputValue);
    }
    return fs.writeFile(file, InputValue, 'utf8', function (err) {
        if (err) throw err;
    });
}


function toDay(days = 0,format=10, char='T'){
    let date = new Date();
    date.setDate(date.getDate() + days);
    date = date.toISOString().slice(0, format).replace(char, '');
    return date;
  }

  function formatDate (){
    const date = new Date();
    const timestamp = date.getTime();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();    
    const formattedDate = `-${day}-${month}-${year}-${timestamp}`;

    return formattedDate;
  };

  function randomDate(format, start, end) {
    const date = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()))
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();

    let formattedDate = '';
    switch (format) {
        case "YYYY/MM/DD 00:00:00":
            formattedDate = `${year}/${month}/${day} ${hours}:${minutes}:${seconds}`;
        break;
        case "DD/MM/YYYY 00:00:00":
            formattedDate = `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;
        break;
        case "YYYY/MM/DD":
            formattedDate = `${year}/${month}/${day}`;
        break;
        case "DD/MM/YYYY":
            formattedDate = `${day}/${month}/${year}`;
        break;
        case "MM/DD/YYYY 00:00:00":
            formattedDate = `${month}/${day}/${year} ${hours}:${minutes}:${seconds}`;
        break;
        case "MM/DD/YYYY":
            formattedDate = `${month}/${day}/${year}`;
        break;
        case "MM-DD-YYYY 00:00:00":
            formattedDate = `${month}-${day}-${year} ${hours}:${minutes}:${seconds}`;
        break;
        case "MM-DD-YYYY":
            formattedDate = `${day}-${month}-${year} ${hours}:${minutes}:${seconds}`;
        break;
        case "YYYY-MM-DD 00:00:00":
            formattedDate = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
        break;
        case "DD-MM-YYYY 00:00:00":
            formattedDate = `${day}-${month}-${year} ${hours}:${minutes}:${seconds}`;
        break;
        case "YYYY-MM-DD":
            formattedDate = `${year}-${month}-${day}`;
        break;
        case "DD-MM-YYYY":
            formattedDate = `${day}-${month}-${year}`;
        break;
    
        default:
            formattedDate = `${year}/${month}/${day}`;
            break;        
    }
    return formattedDate;
  }

  
  
  const validateJsonCreate = file =>{
    let count = Object.keys(file);
    if(count.length === 3){
        if(count[0] === "generate")
            if(count[1] === "table")
                if(count[2] === "data"){
                    file.data.forEach(element => {
                        let data = Object.keys(element)
                        if (data.length >= 4) {
                            if (data[0] === "field" && data[1] === "type" && data[2] === "format" && data[3] === "values") {
                                return 1;              
                            }else{
                                return 0;
                            }
                        }
                    });
                }
                return 1;
    }else{
        return 0;
    }
  };
  

module.exports = {
    readFilesFolder,
    moveFile,
    readFile,
    writeFile,
    toDay,
    formatDate,
    generateBody,
    validateJsonCreate,
    randomDate,
    delay
}





