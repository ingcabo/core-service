
const test = require('../components/test_service/network');
const filesfortest  = require('../components/filesForTest/network');
const getexecute = require('../components/getAutomation/network');
const jokes = require('../components/jokes/network');
const settest = require('../components/fileSetTest/netwoork');
const postmanApi = require('../components/postman-api/network');
const batchValidator = require('../components/batch-processing-validation/network.js'); 
const queryGenerator = require('../components/query-generation/network.js');
const route = "/qa/core"

const routes = function(server){
    server.use(`${route}/jokes`,jokes);
    server.use(`${route}/health`,test);
    server.use(`${route}/filesfortest`,filesfortest);
    server.use(`${route}/getexecute`,getexecute);
    server.use(`${route}/settest`,settest);
    server.use(`${route}/postman-api`,postmanApi);  
    server.use(`${route}/batch-validator`,batchValidator);
    server.use(`${route}/generate-query`,queryGenerator);    
}

module.exports = routes;
