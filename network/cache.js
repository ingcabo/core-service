const NodeCache = require('node-cache');
const logger = require('../network/logger');
const { getTokenAdmin } = require('../components/naranjax_user/auth0_get_token_admin/controller');

const expTime = 86400;
const checkperiodTime = 70000;

const nodeCache = new NodeCache({ stdTTL: expTime, checkperiod: checkperiodTime });

/**
 * @func getMemCache - Obtiene un valor del cache
 * @param {String} key - Clave del valor a recuperar
 * @return {Object} - Valor obtenido del cache
 */

 //deuda tecnica hacerlo mas reutilizable
const getMemCache = async (key) => {
    let value_key = nodeCache.get(key);
    if ( value_key == undefined ){
        console.log("We only enter here when the key does not exist ········#####")
        let {data:access_token} = await get_token();
        setMemCache(key,access_token);
        return access_token;
    }
    
    return value_key;
};

async function get_token(){
    let data = await getTokenAdmin();
    return data;
}


/**
 * @func setMemCache - Guarda un valor en el cache
 * @param {String} key - Clave del valor
 * @param {Object} valueObject - Valor a guardar
 * @return {Boolean} - Valor que indica el éxito de la operación
 */
const setMemCache = (key, valueObj) => nodeCache.set(key, valueObj);

/**
 * @this {Listener} - initialize listener on expired
 */
 //deuda tecnica hacerlo mas reutilizable
nodeCache.on('expired', async () => {
  try {
    console.log("expired token.......")
    let {data:access_token} = await get_token();
    setMemCache('tokenSecurityAdmin', access_token);
  } catch (error) {
    logger.error(
      `Error retrieving token  from Auth0 setMenCache: ${JSON.stringify(error)}`,
    );
  }
});


module.exports = {
  getMemCache,
  setMemCache,
};
