const {STATIC_REPORT_URL} = require("../config");

function setParameters(message,testParams){

    let {summary,failures,timings} = message;
    let {timeForResportTestNameHtml} = testParams;
    
        var result="\u2705";
        var textrsult = " Success Test ";
        var errordetail="";

      if(failures.length > 0){
            errordetail = concaterror(failures);
            result = "\274C";
            textrsult = " Failed Test ";
        }
        
        const URL_REPORT = STATIC_REPORT_URL;
        const TIME_REPORT= timeForResportTestNameHtml;
        const NAME_REPORT = summary.profile.name;
        const name = summary.profile.name;
        const itexc = summary.iterations.executed;
        const itfld = summary.iterations.failed;
        const tscxc = summary.testScripts.executed;
        const tcfld = summary.testScripts.failed;
        const rqsxc = summary.requests.executed;
        const rqfld = summary.requests.failed;
        const prexc = summary.prerequestScripts.executed;
        const prfld = summary.prerequestScripts.failed;
        const asstc=summary.assertions.executed;
        const assfl=summary.assertions.failed;
        const avrtim =timings.responseAverage;
        const truntime =timings.completed-timings.started;
        const date = new Date().toLocaleString();

        let parametersObject = {
            URL_REPORT,TIME_REPORT,NAME_REPORT,name,itexc,itfld,
            tscxc,tcfld,rqsxc,rqfld,prexc,prfld,asstc,assfl,avrtim,
            truntime,date,result,textrsult,errordetail
        }

        return parametersObject;
}


function textSlack(parameters) {

   let  {
        URL_REPORT,TIME_REPORT,NAME_REPORT,name,itexc,itfld,
        tscxc,tcfld,rqsxc,rqfld,prexc,prfld,asstc,assfl,avrtim,
        truntime,date,result,textrsult,errordetail
    }= parameters;

    let data=result +" "+textrsult+" "+name+ "  " +date+`
    • iterations      • requests
    executed: ${itexc}         executed: ${rqsxc}
    failed: ${itfld}               failed: ${rqfld}
    • testScripts     • prerequestScripts
    executed: ${tscxc}         executed: ${prexc}
    failed: ${tcfld}               failed: ${prfld}
    • assertions       • timings
    executed: ${asstc}         average resp time:${avrtim}ms 
    failed: ${assfl}                run duration:${truntime}ms   
    <${URL_REPORT}/${NAME_REPORT}-${TIME_REPORT}.html| Click here to view the html report >
    \n• errordetail
    [${errordetail}]`;
    return data;
}

function textMsTeams(parameters){

    let  {
        URL_REPORT,TIME_REPORT,NAME_REPORT,name,itexc,itfld,
        tscxc,tcfld,rqsxc,rqfld,prexc,prfld,asstc,assfl,avrtim,
        truntime,date,result,textrsult,errordetail
    }= parameters;

    
    let header = headerTest(parameters,result,textrsult);
    let bodyText=textBodyTeams(parameters);
    let footText = footerBody(parameters);
    let body = msTeamBody(footText,header,bodyText);

    return body;
}


function textBodyTeams(parameters) {

    let  {
        URL_REPORT,TIME_REPORT,NAME_REPORT,name,itexc,itfld,
        tscxc,tcfld,rqsxc,rqfld,prexc,prfld,asstc,assfl,avrtim,
        truntime,date,result,textrsult,errordetail
    }= parameters;

    let data= ` 

    • iterations      • requests
    executed: ${itexc}         executed: ${rqsxc}
    failed: ${itfld}           failed: ${rqfld}

    • testScripts     • prerequestScripts
    executed: ${tscxc}         executed: ${prexc}
    failed: ${tcfld}            failed: ${prfld}

    • assertions       • timings
    executed: ${asstc}         average resp time:${avrtim}ms 
    failed: ${assfl}           run duration:${truntime}ms


    \n• errordetail
    [${errordetail}]`;

    return data;
}

function headerTest(messageObject,result,textrsult) {
    let {date,name} = messageObject;
    let data=result +" "+textrsult+" "+name+ "  " +date;
    return data;

}

function footerBody(messageObject){
    let  {
        URL_REPORT,TIME_REPORT,NAME_REPORT
    }= messageObject;

    let foot=`${URL_REPORT}/${NAME_REPORT}-${TIME_REPORT}.html`;
    return foot;
}


function msTeamBody(UrlRepor,titelTest,textSummery){
    let body={
        "@type": "MessageCard",
        "@context": "http://schema.org/extensions",
        "themeColor": "0076D7",
        "summary": "a summary",
        "sections": [{
            "activityTitle": titelTest,
            "activitySubtitle": textSummery,
            "markdown": true
        }],
        "potentialAction": [{
            "@type": "ActionCard",
            "name": "To see the Report click Here!!!",
            "actions": [{
                "@type": "OpenUri",
                "name": "To see the Report click Here!!!",
                    "targets": [
                        { "os": "default", "uri": UrlRepor }
                    ]
            }]
        }]
    }
    return body;
}


function concaterror(data){
   
    var str = " ";
    
    for (i = 0; i < data.length; i++) {
         str += " the operation " +data[i].source.name + " -> "+data[i].error.message+",";
      }
      str = str.substring(0,str.length -1);
    return str;
};


module.exports = {
    setParameters,
    textMsTeams,
    textSlack,
    msTeamBody

}

