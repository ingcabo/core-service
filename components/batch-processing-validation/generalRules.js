
function lengthValidation(value, minLength,maxnLength){
    
    let errorMessage = "";
    if (minLength == 0){
        errorMessage = "";
    } else if(value.length < minLength || value.length > maxnLength){
        errorMessage = "field should be " +minLength+ " to " +maxnLength+ " characters.";
    }else{
        errorMessage = "";
    }
    return errorMessage;
 }

 function lengthValidationObjectJson(object, countColumn){

    let errorMessage = "";
    let count = Object.keys(object).length;

    if (count != countColumn){
        errorMessage = "the number of columns sent is not as expected";
    }

    return errorMessage;
 }

 function validateFormat(value, type, format,min_length,max_length) {
  
   let regExpValidation ="";
   switch (type) {
    case 'date': //date
        regExpValidation = validateDateFormat(value,format,min_length,max_length);
    break;
    case 'string': //uso
        regExpValidation =validateStringFormat(value,min_length,max_length);
    break;
    case 'numeric': //uso
        regExpValidation = validateNumericFormat(value,min_length,max_length);
    break;
    case 'alphanumeric': //uso
        regExpValidation = validateAlphaNumericFormat(value,min_length,max_length);
    break;
    case 'double'://uso
        regExpValidation = validateDouble(value,min_length);
    break;
    case 'amount': //uso
        regExpValidation =validateAmount(value,format,min_length,max_length);
    break;
    case 'tasaEfect':
        //regExpValidation =tasaEfect(value,min_length);   
    break;
   
    default:
        regExpValidation = "default response";
        break;
   }
    return regExpValidation;
};

function validateDateFormat(dateString, format, min_length ,max_length){
    let regExpPassword = "";
    let errorMessage="";
    if(format == "YYYYMMDD"){
        //YYMMDD
        regExpPassword =  /([12]\d{3}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01]))/;
    }else if(format == 'DD/MM/YYYY'){
        //DD/MM/YYYY
        regExpPassword =  /^(?=\d{2}([-.,\/])\d{2}\1\d{4}$)(?:0[1-9]|1\d|[2][0-8]|29(?!.02.(?!(?!(?:[02468][1-35-79]|[13579][0-13-57-9])00)\d{2}(?:[02468][048]|[13579][26])))|30(?!.02)|31(?=.(?:0[13578]|10|12))).(?:0[1-9]|1[012]).\d{4}$/;
    
    }else if(format == "YYYYMM"){
        regExpPassword =  /([12]\d{3}(0[1-9]|1[0-2]))/;
    }else{
        return errorMessage = "Invalid format."
    }
    
    if (dateString.length == max_length) {
        if (!regExpPassword.test(dateString)){
            errorMessage =  errorMessage = "error validating date field";
        }
    }
    

    return errorMessage;
}
//999999999999999,99 o 999999999999999.99
function validateAmount(value, format, min_length, max_length){

    let errorMessage="";
    let formatArray = format.split(",");
    let regExpPassword = new RegExp('(\\d{0,'+formatArray[0].length+'}[\.\,]\\d{1,'+formatArray[1].length+'}$)');
    if(value.length <= max_length && value.length >= min_length){
        if (!regExpPassword.test(value)){
            errorMessage = "error validating amount field format: "+format;
        }
    }
    
    if(min_length == 0 && value.length == 0){
        errorMessage=""
    }

    return errorMessage;
}


function validateAlphaNumericFormat(value,min_length, max_length){
    let errorMessage="";
    let regExpPassword = /^[a-zA-Z\d\-_\s]+$/;
    if(value.length <= max_length){
        if (!regExpPassword.test(value)){
            errorMessage = "it is not alphanumeric";
        }
    }
    

    return errorMessage;
}

function validateNumericFormat(value,min_length,max_length){
    let errorMessage="";
    //let regExpPassword = new RegExp('(\\d{0,'+formatArray[0].length+'}[\.\,]\\d{1,'+formatArray[1].length+'})');
    let regExpPassword = new RegExp('(\\d{'+ min_length +','+max_length+'})');
    
    if(value.length >= min_length && value.length <= max_length){
        if (!regExpPassword.test(value)){
            errorMessage = "it is not numeric.";
        }
    }    

    if(min_length == 0 && value.length == 0){
        errorMessage=""
    }

    return errorMessage;
}

function validateStringFormat(value,min_length,max_length){
    let errorMessage="";
    let regExpPassword = /^[a-zA-Z\d\-ñÑ�\s]+$/;

    if(value.length <= max_length){
        if (!regExpPassword.test(value)){
            errorMessage = "it is not alphabetical format";
        }
    }    

    if(min_length == 0 && value.length == 0){
        errorMessage=""
    }

    return errorMessage;
}
//999.999.999.999,99
function validateDouble(amount,min_length,max_length){

    let regExpPassword = /^[0-9]{1,3}(\.[0-9]{3})*\,[0-9]+$/;
    let errorMessage="";

    if (!regExpPassword.test(amount)){
        errorMessage = "error validating amount field  format 1.000,00";
    }

    if(min_length == 0 && value.length == 0){
        errorMessage=""
    }

    return errorMessage;
}


/*function validateDateFormatV2(dateString){

    let regExpPassword =  /^(?=\d{2}([-.,\/])\d{2}\1\d{4}$)(?:0[1-9]|1\d|[2][0-8]|29(?!.02.(?!(?!(?:[02468][1-35-79]|[13579][0-13-57-9])00)\d{2}(?:[02468][048]|[13579][26])))|30(?!.02)|31(?=.(?:0[13578]|10|12))).(?:0[1-9]|1[012]).\d{4}$/;
    let errorMessage="";

    if (!regExpPassword.test(dateString)){
        errorMessage =  errorMessage = "error validating date field";
    }

    return errorMessage;
}*/

/*function tasaEfect(amount,min_length){

    let regExpPassword = /^^[0-9]{1,3}(\.[0-9]{1,3})+$/;
    let errorMessage="";

    if (!regExpPassword.test(amount)){
        errorMessage = "error validating amount field  format 5.000";
    }

    if(min_length == 0 && value.length == 0){
        errorMessage=""
    }

    return errorMessage;
}*/



/*function validateNumericFormat(value,min_length){
    let errorMessage="";
    let regExpPassword = /^-?\d+$/;

    if (!regExpPassword.test(value)){
        errorMessage = "it is not numeric";
    }

    if(min_length == 0 && value.length == 0){
        errorMessage=""
    }

    return errorMessage;
}*/


module.exports = {
    validateFormat,
    lengthValidationObjectJson,
    lengthValidation,
    validateNumericFormat,
    validateAlphaNumericFormat

}
