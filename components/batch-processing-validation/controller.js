const csvToJson = require('convert-csv-to-json');
const tools = require('../../network/toolbox');
//const { dirBatchfile } = require('../../cfg.json')
const {dirBatchfile} = require('../../config');
const {NORMATIVOS,APER_OBLIGA,OBLIGA_VIGENT,RENOVACION} = require('./constans.js')

const {applyRules} = require('./applyRules.js');

async function generateJsonFileController(file){
    
    setTimeout(function(){
        let jsonFile =  generateJsonFile(file);
        return jsonFile;    
    },2000)
    
}

 async function validationController(object){
    

   let validation = await applyRules(object);
   let sumary = await selectGroupBy(object);
   let sumaryTotal = {
    ...validation,
    sumary
   }

   return sumaryTotal;
}

async function generateBodyController(object){
  const { file_name } = object;
  let body =  await tools.generateBody(file_name);
  let bodyObject = {
    "file_name": "{{v_fileName}}",
    "count_column": body[1],
    "details": body[0]
  };
  return bodyObject;
}


async function selectGroupBy(object){   
    const {file_name } = object;
    let sumary = [];
   
    if (file_name.includes(NORMATIVOS)) {
        console.log(`i am in ${NORMATIVOS}`);
        let packageObj = await JSON.parse(tools.readFile(dirBatchfile + file_name + ".json"));
        sumary =groupByNormativos(packageObj);
    } else if (file_name.includes(APER_OBLIGA)) {

    
        console.log(`${APER_OBLIGA}`);
    } else if (file_name.includes(OBLIGA_VIGENT)) {

        console.log(`${OBLIGA_VIGENT}`);
    } else if (file_name.includes(RENOVACION)) {
        
        console.log(`${RENOVACION}`);
    } else {
        console.log("alternative flow!!!!")
    }
    return sumary;
}

async function generateJsonFile(file_name){
    let fileOutputName = file_name.substring(0- 1, file_name.length-4)+'.json';  
    console.log(fileOutputName)
    csvToJson.fieldDelimiter(';');
    csvToJson.generateJsonFileFromCsv(dirBatchfile+file_name,dirBatchfile+fileOutputName);
    return fileOutputName;
}


function groupByNormativos(long_array){

    long_array.forEach(object => {
        delete object['Valor'];
      });
    
    const unique_values = [...new Map(long_array.map(obj => [JSON.stringify(obj), obj])).values()]; // this will remove duplicate values from long_array
    const result = unique_values.map((val) => { // iterate in unique_values
      let count = 0;
      long_array.forEach((item) => {
        item.SubCuenta == val.SubCuenta  && item.FechaCorte === val.FechaCorte && item.TipoPersona === val.TipoPersona && item.TipoInformacion === val.TipoInformacion && count++
      }); //iterate in long_array and count same values
      return { ...val,
        count: count
      }
    })

    return result;
}


function groupByNormativos22(long_array){

    long_array.forEach(object => {
        delete object['Valor'];
      });
    
    const unique_values = [...new Map(long_array.map(obj => [JSON.stringify(obj), obj])).values()]; // this will remove duplicate values from long_array
    const result = unique_values.map((val) => { // iterate in unique_values
      let count = 0;
      long_array.forEach((item) => {
        item.SubCuenta == val.SubCuenta  && item.FechaCorte === val.FechaCorte && item.TipoPersona === val.TipoPersona && item.TipoInformacion === val.TipoInformacion && count++
      }); //iterate in long_array and count same values
      return { ...val,
        count: count
      }
    })

    return result;
    
}

module.exports = {
    generateJsonFileController,
    validationController,
    generateBodyController
}



