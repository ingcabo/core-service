const db = require('mongoose');
const Model = require('./model');

db.Promise = global.Promise;
db.connect('mongodb+srv://<user>:<pass>@casslos-nzves.mongodb.net/test?retryWrites=true&w=majority',{
    useNewUrlParser:true,
    useUnifiedTopology:true,
});
console.log('db conectada ....')

function addMessage(message){
    const myMessage = new Model(message);
    myMessage.save();
}

async function getMessage(filterUser){
    let filter = {};

    if (filterUser !== null) {
         filter = {user: filterUser};
    }   

    const messages = await Model.find(filter);
    return messages;
}

async function updateText(id, message) {
    const founMessage = await Model.findById({
     _id : id
 });

 founMessage.message= message;
 const newMessage = founMessage.save();
 return newMessage;
}

async function removeMessage(id){
   return Model.deleteOne({
        _id:id
    })

}

module.exports = {
    add:addMessage,
    list:getMessage,
    updateText: updateText,
    remve:removeMessage,
    //get
    //update
    //delete
}