const store = require('./store');

function addMessage(user,message){

 console.log('controllet datos: '+ ' '+user + ' ' + message );
return new Promise((resolve,reject) => {

  if (!user || !message){
     console.log('[messageController]: ' + 'no user or message');   
     reject('incomplete');
     return false;
    }
    const fullMessage = {
        user:user,
        message:message,
        date: new Date(),
    };
  
   
    resolve(fullMessage);
    console.log(fullMessage);
    store.add(fullMessage);
});

};

function getMessages(filterUser){
    return new Promise((resolve,reject)=>{
       resolve(store.list(filterUser));
    })
}

function updateMessage(id,message){
    return new Promise( async (resolve,reject) =>{
        if(!id || !message){
            reject('invalid data');
            return false;

        }
        const result = await store.updateText(id,message);

        resolve(result);
    })
}


function deleteMessage(id){
    return new Promise ((resolve,reject) =>{

        if (!id){
            reject('id invalido al eliminar');

        }
        store.remove(id)
        .then(()=>{
            resilve();
        }).catch((e)=>{
            reject(e);
        })

        
    })
}

module.exports = {
    addMessage,
    getMessages,
    updateMessage,
    deleteMessage

}