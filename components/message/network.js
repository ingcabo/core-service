const express = require('express');
const response = require('../../network/response');
const controller = require('./controller');
const router = express.Router();


router.get('/',function(req,res){
    const filterMessage = req.query.user || null;
   controller.getMessages(filterMessage)
   .then((messageList)=>{
    response.ssuccess(req,res,messageList,200);
   }).catch(e => {
    response.error(req, res, 'Informacion invalida get', 500, e );
   })
});

router.post('/',function(req,res){
  /*  res.header({
        'custom-header':'valor personalizado'
    })*/
    console.log('usuario: ' + req.body.user + ' menssage: '+ req.body.message);
    controller.addMessage(req.body.user,req.body.message)
        .then((fullMessage)=>{
            response.ssuccess(req,res,fullMessage,201);
        })
       .catch(e =>{
        response.error(req, res, 'Informacion invalida', 400, 'Error en el controlador');
       });
});

router.patch('/:id', function(req,res){
    console.log(req.params.id);
    controller.updateMessage(req.params.id,req.body.message)
    .then((data)=> {
        response.ssuccess(req,res,data,200);
    }).catch(e =>{
        response.error(req, res, 'Informacion invalida actualix¡zar', 500, e);
    });
});

router.delete('/:id',function(req,res){
    controller.deleteMessage(req.params.id).then(()=>{
        response.ssuccess(req,res,`registro ${req.params.id} eliminado`,200);
    }).catch(e =>{
        response.error(req, res, 'error interni', 500, e);
    })
   
});


module.exports = router;