
const fs = require('fs');
const path = require('path');
//var file = "jokes.txt";
//let fileJoke =  fs.readFileSync(path.join(__dirname, file), 'utf8');

function contLineFile(file){
    //pendiente hacer metodo dinamico
    var lines = 5; 
    return lines;
}


function randomNum(n) {
  let max = n;
  let min = 1
  return new Promise((resolve,reject) =>{
    let rand = Math.floor(Math.random() * (max - min + 1) + min);
    resolve(rand);
  })
}

function get_line(filename, line_no, callback) {
    const config= path.resolve(filename);
    fs.readFile(config, function (err, data) {
      if (err) throw err;
 
      var lines = data.toString('utf-8').split("\n");
 
      if(+line_no > lines.length){
        return callback('File end reached without finding line', null);
      }
 
      callback(null, lines[+line_no]);
    });
}


 module.exports = {
  contLineFile,
  randomNum,
  get_line
}


//randomNum(contLineFile(file));