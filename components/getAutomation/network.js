const express = require('express');
const response = require('../../network/response');
const logger = require('../../network/logger');
const router = express.Router();
const tools = require('../../network/toolbox.js');
const summaryReport = require('../../htmlreport/summaryReport.js')
const controller = require('../../controllers/controller');
const mongoDbClient = require('../../controllers/db-controller');
const fs = require('fs');
//const { dirTesfile, fileTestName, dirReport } = require('../../cfg.json');
const {dirTesfile,fileTestName,dirReport,WEBHOOK_TYPE,MONGO_DB_TEST_COLL,MONGO_DB_TESTRUNS_COLL} = require('../../config');
const jsonFileTestsStorage = `${dirTesfile}/${fileTestName}`;

router.get('/', async function (req, res) {
    var testName = "";
    if (!req.query.test) {

        var msg = "specify ?test=testname parameters";
        // response.msgSlack(msg);
        sendMssge(msg);
        response.error(req, res, msg, 500, msg);

    } else {
        let query = { _id: req.query.test }
        var fullUrl = req.protocol + '://' + req.get('host');
        mongoDbClient.GetOne(MONGO_DB_TEST_COLL, query).then((resp)=>{
            let test = resp[resp._id];
            for (var i = 0; i < test.length; i++) {
                let timeForResportTestNameHtml = tools.formatDate();
                let testParams = { ...test[i], timeForResportTestNameHtml }
                let UrlTestReport = `${fullUrl}/${testParams.name}-${timeForResportTestNameHtml}.html`;
                let UrlTestReportName = `${testParams.name}-${timeForResportTestNameHtml}`;
                controller.runcollection(testParams).then((result) => {
                    logger.info(JSON.stringify(result));
                    let collectionName = result.collection.name;
                    var data = {
                        summary: summaryReport.prepareData(result.run.stats, collectionName),
                        failures: result.run.failures,
                        timings: result.run.timings,
                        runFile: UrlTestReportName
                    };

                    //we create the html to see the reports
                    summaryReport.wirteHtmlReportFile(dirReport);
                    //we are sending de report summary by Slack Web hooks
                    sendMssge(data, testParams);
                    let query =  { _id: UrlTestReportName,date: new Date(), testName: UrlTestReportName, url: UrlTestReport}
                    mongoDbClient.Create(MONGO_DB_TESTRUNS_COLL, query).then((result=>{
                        //response.success(req,res,"We stored the Newman results Set!, _id: " + result.insertedId,200);    
                    })).catch((err)=>{
                        response.error(req, res, err.message, 500, err.stack);
                    });
                    response.success(req, res, data, 200);

                }).catch(err => {
                    logger.error(err.message)
                    logger.error(err.stack)
                    response.error(req, res, err.message, 500, err.stack);
                })
            }
        }).catch((err=>{
            response.error(req,res,err.message,500,err.stack);
        }));
    }

});

router.get('/getfiles', (req, res) => {
    let result = "";
    mongoDbClient.GetAll(MONGO_DB_TESTRUNS_COLL).then((resp)=>{
        if(resp.length === 0){
            result = {message: "The collection is empty or inexistent!!"};  
        }else{
            
            result = resp;
        }
        response.success(req,res,result,200);
    }).catch((err=>{
        response.error(req,res,err.message,500,err.stack);
    }))
});

router.post('/', async function (req, res) {
    var testName = "";

    if (!req.body.text) {

        var msg = "specify ?test=testname parameters";
        //enviamos un texto a slack
        // response.msgSlack(msg);
        sendMssge(msg);
        response.error(req, res, msg, 500, msg);

    } else {

        const data = fs.readFileSync('./testfile/profile.json');
        var profile = JSON.parse(data);
        //console.log(profile);
        testName = req.body.text.trim();

        if (!profile[testName]) {

            var msg = "=testname name doesn't exist!!!";
            // response.msgSlack(msg);
            sendMssge(msg);
            response.error(req, res, msg, 500, msg);

        } else {

            profile = profile[testName];
            for (var i = 0; i < profile.length; i++) {
                profiled = profile[i];
                await controller.runcollection(profiled).then(result => {

                    var data = {
                        summary: tools.prepareData(result.stats, profiled),
                        failures: result.failures,
                        timings: result.timings
                    };

                    sendMssge(data);
                    response.success(req, res, data, 200);
                }).catch(err => {
                    response.error(req, res, err, 500, err);
                })

            }

        }

    }

});

function sendMssge(msg, testParams = "") {
    //const WEBHOOK_TYPE = 3; //aplicar mejores practicas, colocarlo como variable de entorno de SO
    response.msgTeams(msg, testParams);
    if (WEBHOOK_TYPE == 2) {
        console.log(" WEBHOOK_TYPE  entramos aca 22 =========")
        response.msgTeams(msg, testParams);
    } else if (WEBHOOK_TYPE == 1) {
        response.msgSlack(msg, testParams);
        console.log(" WEBHOOK_TYPE  entramos aca 1 =========")
    } else {
        console.log(" WEBHOOK_TYPE  entramos aca 3 =========")
        response.msgTeams(msg, testParams);
        response.msgSlack(msg, testParams);
    }

}

module.exports = router;


