const express = require('express');
const {resolve} = require('path');
const response = require('../../network/response');
const controller = require('./controller');
const tools = require('../../network/toolbox');
const {dirGenerateQueryfile} = require('../../config');
const fs  = require('fs');
const router = express.Router();

router.post('/insert', async (req, res) => {
  let queryString;
    try {

      if (!req.files) {
        response.error(req, res, "you must select files to upload Json create Format", 500, "you must select files to upload");
      } else {
  
        if (req.files.create_file) {
          let file = req.files.create_file;
          var fileData = await tools.moveFile(file, dirGenerateQueryfile);
          console.log(fileData);
          queryString = await controller.processFile(fileData);
          tools.writeFile(await queryString, dirGenerateQueryfile + "query-generado.sql");
          let absolutPath = resolve(dirGenerateQueryfile +"query-generado.sql");
          const rs = fs.createReadStream(absolutPath);

          // set response header: Content-Disposition
          res.setHeader('Content-Type', 'application/sql');
          res.setHeader('Content-Disposition', 'inline; filename="query-generado.sql"');
          // pipe the read stream to the Response object
          rs.pipe(res);
          //res.download(dirGenerateQueryfile+"query-generado.sql");
          console.log(absolutPath);
          //res.sendFile(rete);

          //response.success(req, res, rete, 200);
        }
  
      }
    } catch (err) {
      response.error(req, res, err.message, 500, err.stack);
    }
  });

  module.exports = router;