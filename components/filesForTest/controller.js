const csvToJson = require('convert-csv-to-json');
const tools = require('../../network/toolbox');
//const { dirData, dirEnv, dirFeat, dirTesfile } = require('../../cfg.json') 
const {dirData,dirEnv,dirFeat,dirTesfile} = require('../../config');


function generateJsonFile(filename){
    let fileOutputName = filename.substring(0- 1, filename.length-4)+'.json';  
    csvToJson.generateJsonFileFromCsv(filename,fileOutputName);
    return fileOutputName;
}

async function allFile(){

    var fileData  = await tools.readFilesFolder(dirData);
    var fileEnv   = await tools.readFilesFolder(dirEnv);
    var fileColle = await tools.readFilesFolder(dirFeat);
    var fileTest  = await tools.readFilesFolder(dirTesfile);
    
    var datos = {
        data:fileData,
        environment:fileEnv,
        features:fileColle,
        filetest:fileTest
    }
    return datos;
}   



module.exports = {
    generateJsonFile,
    allFile
}



