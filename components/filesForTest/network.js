const express = require('express');
const response = require('../../network/response');
const controller = require('./controller');
const tools = require('../../network/toolbox');
//const { dirData, dirEnv, dirFeat, dirTesfile } = require('../../cfg.json')
const {dirData,dirEnv,dirFeat,dirTesfile} = require('../../config');
const router = express.Router();

router.post('/', async (req, res) => {
  try {
    if (!req.files) {
      response.error(req, res, "you must select files to upload data,env,collection,filetest", 500, "you must select files to upload");
    } else {

      var value = 0;
      var uploadFiles = {};

      if (req.files.filetest) {
        let file = req.files.filetest;
        var fileData = tools.moveFile(file, dirTesfile);
        uploadFiles = { ...uploadFiles, filetest: fileData };
        ++value;
      }
      if (req.files.data) {
        let data = req.files.data;
        var fileData = tools.moveFile(data, dirData);
        uploadFiles = { ...uploadFiles, iterationData: fileData };
        ++value;
        //controller.generateJsonFile(fileData);
      }
      if (req.files.env) {
        let env = req.files.env;
        var fileEnv = tools.moveFile(env, dirEnv);
        uploadFiles = { ...uploadFiles, environment: fileEnv };
        ++value;
      }
      if (req.files.collection) {
        let coll = req.files.collection;
        var collection = tools.moveFile(coll, dirFeat);
        uploadFiles = { ...uploadFiles, collection: collection };
        ++value;
      }
      uploadFiles = { ...uploadFiles, countFiles: value }
      response.success(req, res, uploadFiles, 200);

    }
  } catch (err) {
    response.error(req, res, err, 500, err);
  }
});

router.get('/', (req, res) => {
  controller.allFile().then((result => {
    response.success(req, res, result, 200);
  })).catch((err => {
    response.error(req, res, err, 500, err);
  }))

});

module.exports = router;

