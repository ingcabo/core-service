const express = require('express');
const app = express();
app.use('/static', express.static('public'))
const response = require('../../network/response');
const controller = require('./controller');
const mongoDbClient = require('../../controllers/db-controller');
const {dirStoreNewmanResults,dirTesfile,fileTestName,MONGO_DB_TEST_COLL,MONGO_DB_NEWMANRESULTS_COLL} = require('../../config');
//const cfg =require('../../cfg.json');
const jsonFileTestsStorage = `${dirTesfile}/${fileTestName}`;
const router = express.Router();
const tools = require('../../network/toolbox'); 

router.post('/', async(req, res) => {  
    let obj="";
    if (!req.body){
        response.error(req, res, "there is no json in the message", 500, "there is no json in the message");
    } 
    obj = req.body;
    if(!mongoDbClient.isObject(obj)){
        throw Error("mongoClient.insertDocumentWithIndex: document is not an object");
        return("mongoClient.insertDocumentWithIndex: document is not an object");
    }
    let id = Object.keys(obj)[0];
    let doc = obj[id];
    try{   
        let query =  { 
            _id: id,
            date: new Date(),
            [id]: doc
        }     
        mongoDbClient.Create(MONGO_DB_TEST_COLL, query).then((result=>{
            response.success(req,res,"We created the Test Set!, _id: " + result.insertedId,200);    
        })).catch((err)=>{
            response.error(req, res, err.message, 500, err.stack);
        })
    }catch (err){
        response.error(req, res, err, 500, err);
    }
});
router.post('/store-nr', async(req, res) => {      
    try {

        if (!req.files || req.body.testName === '') {
          response.error(req, res, "You must select files to upload Json create Format and choose a name for the record.", 500, "you must select files to upload");
        } else {
    
          if (req.files.store) {
            let file = req.files.store;
            var fileData = await tools.moveFile(file, dirStoreNewmanResults);
            console.log(fileData);
            let name = req.body.testName;
            const {id, data} = await controller.processFile(fileData);
            let query =  { 
                _id: id,
                date: new Date(),
                testName: name,
                ...data
            }
            mongoDbClient.Create(MONGO_DB_NEWMANRESULTS_COLL, query).then((result=>{
                response.success(req,res,"We stored the Newman results Set!, _id: " + result.insertedId,200);    
            })).catch((err)=>{
                response.error(req, res, err.message, 500, err.stack);
            });
  
            //response.success(req, res, "message", 200);
          }
    
        }
      } catch (err) {
        response.error(req, res, err.message, 500, err.stack);
      }
});
router.get('/list-nr', (req, res) => {
    let result = "";
    mongoDbClient.GetAll(MONGO_DB_NEWMANRESULTS_COLL).then((resp)=>{
        if(resp.length === 0){
            result = {message: "The collection is empty or inexistent!!"};  
        }else{
            result = resp;
        }
        response.success(req,res,result,200);
    }).catch((err=>{
        response.error(req,res,err.message,500,err.stack);
    }))
});

router.get('/listone-nr', (req, res) => {
    let result = "";
    let query = '';
   
    if (req.query.testname) {
        query = { testName: req.query.testname }
        mongoDbClient.GetOne(MONGO_DB_NEWMANRESULTS_COLL, query).then((resp)=>{
            if(resp.length === 0){
                result = {message: "The collection is empty or inexistent!!"};
            }else{
                response.success(req,res,resp,200);
            }
        }).catch((err=>{
            response.error(req,res,err,500,err);
        }));    
    }else{
        result = {message: "You have to provide a name test to find."}
        response.success(req,res,result,200);
    }
    
  });

router.get('/', (req, res) => {
    let result = "";
    let query = '';
    
    if (req.query.testname) {
        query = { _id: req.query.testname }
        mongoDbClient.GetOne(MONGO_DB_TEST_COLL, query).then((resp)=>{
            if(resp.length === 0){
                result = {message: "The collection is empty or inexistent!!"};
            }else{
                result = resp[req.query.testname];
                response.success(req,res,result,200);
            }
        }).catch((err=>{
            response.error(req,res,err,500,err);
        }));    
    }else{
        result = {message: "You have to provide a name test to find."}
        response.success(req,res,result,200);
    }
    
    
  });

router.get('/all', (req, res) => {
    let result = "";
    mongoDbClient.GetAll(MONGO_DB_TEST_COLL).then((resp)=>{
        result = resp;
        if(resp.length === 0){
            result = {message: "The collection is empty or inexistent!!"};
            response.success(req,res,result,200);
        }else{
            response.success(req,res,result,200);
        }
        
    }).catch((err=>{
        response.error(req,res,err.message,500,err.stack);
    }))
});
  

router.put('/', async(req, res) => {  
    let obj="";
    if (!req.body){
        response.error(req, res, "there is no json in the message", 500, "there is no json in the message");
    } 
    obj =req.body;
    if(!mongoDbClient.isObject(obj)){
        throw Error("mongoClient.insertDocumentWithIndex: document is not an object");
        return("mongoClient.insertDocumentWithIndex: document is not an object");
    }
    let id = obj.id;
    let doc = obj.data;
    if(id){
        try{        
            mongoDbClient.Update(MONGO_DB_TEST_COLL, id, doc).then((result=>{
                response.success(req,res,"We modified the Test Set!, id: " + id,200);    
            })).catch((err)=>{
                response.error(req, res, err.message, 500, err.stack);
            })
        }catch (err){
            response.error(req, res, err.message, 500, err.stack);
        }
    }else{
        response.error(req, res, "missing id parameter", 401, "missing id parameter"); 
    }
    
});

router.delete('/:id', (req, res) => {
    if (req.params.id){
        mongoDbClient.Delete(MONGO_DB_TEST_COLL, req.params.id).then((result=>{
            response.success(req,res,"Set Removed!!, _id: "+ req.params.id,200);    
         })).catch((err)=>{
            response.error(req, res, err.message, 500, err.stack);
         })
       
    }else{
        response.error(req, res, "missing id parameter", 401, "missing id parameter");
    }  
});
app.get('/static', (req, res) => {
    res.sendFile('./landing-page/about.html', { root: __dirname });
});

  module.exports = router;
