const tools = require('../../network/toolbox'); 

function getFileTest(file) {
    return new Promise((resolve, reject) => {
        let data = JSON.parse(tools.readFile(file));
        resolve(data);
    })
}

//creating a new test
function createFile(obj, file) {
    return new Promise((resolve, reject) => {
        let data = tools.readFile(file);
        if (data.length > 0) {
            data = JSON.parse(data);
            data = { ...data, ...obj };
        } else {
            data = obj;
        }
        tools.writeFile(data,file);
        resolve("set created");
    });
}

//deleting a test
function deleteFromObjet(tesName, file) {
    return new Promise((resolve, reject) => {
        data = JSON.parse(tools.readFile(file));
        delete data[tesName];
        tools.writeFile(data,file);
        resolve("set removed");
    });
}
async function processFile(file){
    let packageObj = "";
    let id = "";
    try {
        await tools.delay(10000);
        packageObj = await JSON.parse(tools.readFile(file));
        id = packageObj.globals.id;
    } catch (err) {
        console.log(err);
        
    }
    return {
        "id": id,
        "data":packageObj
    };
    
}

module.exports = {
    createFile,
    getFileTest,
    deleteFromObjet,
    processFile
}