const axios = require('axios');
const AxiosLogger = require('axios-logger')
const logger = require('../../network/logger')

const {POSTMAN_API_KEY,POSTMAN_API_URL} = require('../../config');

const COLLECTIONS_PATH = '/collections'
const ENVIRONMENTS_PATH = '/environments'

const postmanApiClient = axios.create({
  baseURL: POSTMAN_API_URL,
  headers: {
      'x-api-key': POSTMAN_API_KEY
  }
})



postmanApiClient.interceptors.response.use(AxiosLogger.responseLogger);

const collections = async function(search={}){

  return await postmanApiClient.get(COLLECTIONS_PATH, {
    transformResponse: [
      (data) => {
        let resp
        try {
          resp = JSON.parse(data)
          
        } catch (error) {
          throw Error(`[requestClient] Error parsing response JSON data - ${JSON.stringify(error)}`)
        }
        return resp.collections.filter(item => {
          
          return !(search.name) || (item.name.replace(/\s+/g, '') === search.name)
        });
      }
    ]
  });
};

const collection = async function(uid){
  return await postmanApiClient.get(COLLECTIONS_PATH + "/" + uid);
}

const environments = async function(search={}){
  return await postmanApiClient.get(ENVIRONMENTS_PATH, {
    transformResponse: [
      (data) => {
        let resp
        try {
          resp = JSON.parse(data)
        } catch (error) {
          throw Error(`[requestClient] Error parsing response JSON data - ${JSON.stringify(error)}`)
        }
        return resp.environments.filter(item => {
          return !(search.name) || (item.name.replace(/\s+/g, '') === search.name)
        });
      }
    ]
  });
};

const environment = async function(uid){
  return await postmanApiClient.get(ENVIRONMENTS_PATH + "/" + uid);
}

const collectionUrl = (uid) => {
  return new URL(COLLECTIONS_PATH + '/' + uid + '?apikey=' + POSTMAN_API_KEY, POSTMAN_API_URL).href
}

const environmentUrl = (uid) => {
  return new URL(ENVIRONMENTS_PATH + '/' + uid + '?apikey=' + POSTMAN_API_KEY, POSTMAN_API_URL).href
}

module.exports = {
  collections,
  collection,
  environments,
  environment,
  collectionUrl,
  environmentUrl
}