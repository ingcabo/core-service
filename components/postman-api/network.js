const express = require('express');
const response = require('../../network/response');
const controller = require('./controller');
const router = express.Router();

const logger = require('../../network/logger')



router.get('/collections', (req, res) => {
    controller.collections(req.query).then(result =>{
        response.success(req, res, result.data, 200);
    }).catch((err=>{
        response.error(req, res, err, 500,err);
    }))
  
});

router.get('/collections/:uid', (req, res) => {
    controller.collection(req.params.uid).then(result =>{
        response.success(req, res, result.data, 200);
    }).catch((err=>{
        response.error(req, res, err, 500,err);
    }))
  
});

router.get('/environments', (req, res) => {
    controller.environments(req.query).then(result =>{
        response.success(req, res, result.data, 200);
    }).catch((err=>{
        response.error(req, res, err, 500,err);
    }))
  
});

router.get('/environments/:uid', (req, res) => {
    controller.environment(req.params.uid).then(result =>{
        response.success(req, res, result.data, 200);
    }).catch((err=>{
        response.error(req, res, err, 500,err);
    }))
  
});


module.exports = router;