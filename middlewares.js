const compression = require('compression');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const loggerMiddleware = require('./middlewares/logger_middleware')
const basicAuthMiddleware = require('./middlewares/basic_auth_middleware')
const erroHandlerMiddleware = require('./middlewares/error_handler_middleware')

const middlewares = function(app) {
  app.use(loggerMiddleware);
  app.use(basicAuthMiddleware);

  app.use(fileUpload({
    createParentPath: true
  }));

  app.use(bodyParser.json({
    limit: "50mb",
  }));

  app.use(bodyParser.urlencoded({
    limit: "50mb", 
    extended: true, 
    parameterLimit:50000
  }));

  app.use(compression({
    filter: function(req, res) {
     return (/json|text|javascript|css|font|svg/)
     .test(res.getHeader('Content-Type'));
    },
    level: 4,
   }));

   app.use(erroHandlerMiddleware);
}

module.exports = middlewares