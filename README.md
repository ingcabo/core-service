#  automation service postman

the **automation service postman** allows us to consume postman resources,

### Starting 🚀
These instructions will allow you to get a copy of the project running on your local machine for development and testing purposes.

See Deployment to know how to deploy the project.

### Prerequisites 📋
What things do you need to install the software and how to install them.

- [Git Bash](https://www.npmjs.com/get-npm).
- [Node](https://nodejs.org/en/download/).
- [npm](https://www.npmjs.com/get-npm).
- [nodemon](https://www.npmjs.com/package/nodemon).

What things do you need to configure the software.

- [Postman API key](https://company.postman.co/settings/me/api-keys)..
- Authorization token: Basic xxxxxxxxxx
- WEBHOOK_URL_PATH

which are the environment variables that I need to initialize.
```
export POSTMAN_API_KEY="yourPostmanApiKey"
export AUTH_WEB_APP="Basic TokenService"
export WEBHOOK_URL_PATH="https://hooks.slack.com/services/XXXXXXX/"
export STATIC_REPORT_URL="http://{{automation_service_postman}}/app/index.html" 
export POSTMAN_API_URL="https://api.getpostman.com/"

 ```
 
## run the aplication dev

```nodemon server.js```


## Create Docker image

sudo docker build -t automation-service:service-postman .

## Create Docker container

sudo docker run -it  -d -p 3001:3001 --name automation-service-local  --env-file ./env.list  -v `pwd`/:/usr/src/app automation-service:service-postman

sudo docker run -it  -d -p 3001:3001 --name automation-service-local  --env-file ./env.list  automation-service:service-postman

or  Docker Compose 

[see the REadme.md from repository bot_service](https://github.com//bot_service)




## UML diagrams

```
sequenceDiagram
    participant bot_service
    participant automation_service_postman
    participant POSTMAN_API
    
bot_service->>automation_service_postman: URL_WEB_APP
automation_service_postman-->>POSTMAN_API: POSTMAN_API_URL
automation_service_postman-->bot_service: CRUD

 ```
 
### upload data files

We can upload JSON or CSV file to be used as data source when running multiple iterations on a collection.

```
curl --location -g --request POST '{{automation_service_url}}/filesfortest' \
--header 'Authorization: {{basic_auth_automation_service}}' \
--form 'data=@"./pre_data.json"'
```

we can consult the available files

```
curl --location -g --request GET '{{automation_service_url}}/filesfortest' \
--header 'Authorization: {{basic_auth_automation_service}}'
```

### available test query

```
curl --location -g --request GET '{{automation_service_url}}/settest' \
--header 'Authorization: {{basic_auth_automation_service}}'
```

we can delete a test

```
curl --location -g --request DELETE '{{automation_service_url}}/settest/{{testName}}' \
--header 'Authorization: {{basic_auth_automation_service}}'
```


 

### Project tree

 * [data](https://github.com/equinox-platformx/automation_service_postman/tree/main/data) the location of data files test
 * [testfile](https://github.com/equinox-platformx/automation_service_postman/tree/main/testfile) the location of the test settings 
 * [public](https://github.com/equinox-platformx/automation_service_postman/tree/main/public) the location of the html reports
 * [components](https://github.com/equinox-platformx/automation_service_postman/tree/main/components)
   * [postman-api](https://github.com/equinox-platformx/automation_service_postman/tree/main/components/postman-api) postman query methods
 * [controllers](https://github.com/equinox-platformx/automation_service_postman/tree/main/controllers) newman library methods
 * [README.md](https://github.com/equinox-platformx/automation_service_postman/blob/main/README.md)
