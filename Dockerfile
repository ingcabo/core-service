FROM node:16

ENV WEB_PORT_APP=3001
ENV STATIC_REPORT_URL=http://localhost:3001/app/index.html
ENV POSTMAN_API_URL=https://api.getpostman.com/

# Create app directory
WORKDIR /usr/src/app

RUN apt-get update
RUN apt-get install curl -y
RUN apt-get install procps -y
RUN apt-get install bash -y
RUN npm install pm2 --location=global

# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . /usr/src/app

RUN npm install

EXPOSE 3001
CMD [ "node", "server.js" ]
