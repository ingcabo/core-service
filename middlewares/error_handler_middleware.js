
const logger = require('../network/logger')

module.exports = function(err, req, res, next) {
  logger.error(err.errorMessage);
  logger.error(err.stack);
  res.status(500).send({
    errorMessage: err,
    errorStack: err.stack
});
}